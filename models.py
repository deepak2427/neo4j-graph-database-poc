from bulbs.model import Node, Relationship
from bulbs.property import String, Integer, DateTime
from bulbs.utils import current_datetime

# vertices
class User(Node):
    element_type = "user"
    uid = String(nullable=False)
    name = String(nullable=False)
    timestamp = DateTime(default=current_datetime, nullable=False)

class Tag(Node):
    element_type = "tag"
    tid = String(nullable=False)
    name = String(nullable=False)
    timestamp = DateTime(default=current_datetime, nullable=False)

class Post(Node):
    element_type = "post"
    uid = String(nullable=False)
    pid = String(nullable=False)
    content = String(nullable=False)
    timestamp = DateTime(default=current_datetime, nullable=False)


# edges
class Feed(Relationship):
    label = "feed"
    timestamp = DateTime(default=current_datetime, nullable=False)

class Follows(Relationship):
    label = "follows"
    timestamp = DateTime(default=current_datetime, nullable=False)

class Created(Relationship):
    label = "created"
    tid = String(nullable=False)
    timestamp = DateTime(default=current_datetime, nullable=False)

class Neolink(Relationship):
    label = "neolink"
    timestamp = DateTime(default=current_datetime, nullable=False)

class TM(Relationship):
    label = "tm"
    value = Integer(default=-3)
    timestamp = DateTime(default=current_datetime, nullable=False)

class TMSI(Relationship):
    label = "tmsi"
    value = Integer(default=-1)
    timestamp = DateTime(default=current_datetime, nullable=False)
