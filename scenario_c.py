from graph_api import *
import sys
import time    
from contextlib import contextmanager

@contextmanager
def measureTime(count, meta=""):
	t1 = time.clock()
	yield
	t2 = time.clock()
	print '%d %f %s' % (count, t2-t1, meta)


def main():
	g = init_graph()

	if len(sys.argv) < 2:
		print "Provide post count as argument"
		sys.exit(1)

	post_count = int(sys.argv[1])
	tag_count = 100


	#INSERTING DATA
	with measureTime(0, "ADD_USER"):
		add_user("u" + str(0))


	#print "Add second user and insert data..."
	# add first user posts
	for u in range(1, 2):
		with measureTime(u, "ADD_USER"):
			add_user("u" + str(u))

		for t in range(0, 1):
			for p in range(0, post_count):
				with measureTime(p, "ADD_POST"):
					add_post("u" + str(u), "t" + str(t), "p" + str(p) + "t" + str(t))

	# u1 follow t1
	add_follows("u0", "t0")
	#print "Done..."


	# DUMMY QUERYING TO WARM UP DATABASE
	script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

	params = dict(nodeid=1, tmsi=-1)
	nodes = g.cypher.query(script, params)
	
	script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

	params = dict(nodeid=1, tmsi=-1)
	nodes = g.cypher.query(script, params)



	# QUERYING
	#print "Start querying..."
	for t in range(0, tag_count):

		nodes = None 
		with measureTime(t, "RETRIEVE"):
			script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

			params = dict(nodeid=1, tmsi=-1)
			nodes = g.cypher.query(script, params)
			#print len(list(nodes))

		# increase the tag count and add posts for user u1
		if t < tag_count - 1:
			for p in range(0, post_count):
				add_post("u1" , "t" + str(t + 1), "p" + str(p) + "t" + str(t))



	# TESTING
	script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

	params = dict(nodeid=1, tmsi=-1)
	nodes = g.cypher.query(script, params) 
	if not len(list(nodes)) == post_count:
			print "Error!!! --- Test Failed!!!"
			exit(1)

	#print g.V
	#print g.E

if __name__ == "__main__":
	main()
