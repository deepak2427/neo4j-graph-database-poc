from graph_api import *
import sys
import time    
from contextlib import contextmanager

@contextmanager
def measureTime(count, meta=""):
	t1 = time.clock()
	yield
	t2 = time.clock()
	print '%d %f %s' % (count, t2-t1, meta)


def main():
	g = init_graph()

	if len(sys.argv) < 3:
		print "Provide post count and factor as arguments"
		sys.exit(1)

	count = int(sys.argv[1])
	factor = int(sys.argv[2])


	#INSERTING DATA
	with measureTime(0, "ADD_USER"):
		add_user("u" + str(0))


	#print "Add second user and insert data..."
	# add first user posts
	for u in range(1, 2):
		with measureTime(u, "ADD_USER"):
			add_user("u" + str(u))

		for t in range(0, 1):
			for p in range(0, count):
				with measureTime(p, "ADD_POST"):
					add_post("u" + str(u), "t" + str(t), "p" + str(p))

	# u1 follow t1
	add_follows("u0", "t0")
	#print "Done..."


	# DUMMY QUERYING
	script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

	params = dict(nodeid=1, tmsi=-1)
	nodes = g.cypher.query(script, params)
	
	script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

	params = dict(nodeid=1, tmsi=-1)
	nodes = g.cypher.query(script, params)



	# QUERYING
	#print "Start querying..."
	for i in range(0, count + 1, count/factor):

		nodes = None 
		with measureTime(i, "RETRIEVE"):
			script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

			params = dict(nodeid=1, tmsi=-1)
			nodes = g.cypher.query(script, params)
			#print len(list(nodes))

		# mark irrelevant
		if i < count:
			for j in range(i, i + count/factor):
				mark_irrelevant("u0", "p" + str(j))


	# TESTING
	script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

	params = dict(nodeid=1, tmsi=-1)
	nodes = g.cypher.query(script, params) 
	for n in nodes:
		for i in range(0, count + 1, count/factor):
			post_content = "p" + str(i)
			# the nodes should never contain the irrelevant post
			if n.content == post_content:
				print "Error!!! --- Test Failed!!!"
				exit(1)

	#print g.V
	#print g.E

if __name__ == "__main__":
	main()
