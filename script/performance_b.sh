#!/bin/bash
RESULTS_DIR="./results_b"
NEO_HOME="/Users/mark.galea/Dev/Tools/neo4j-community-1.8/"
NE0_DATA_DIR="./data/graph.db"

if [ -e $RESULTS_DIR ]
then
	echo "RESULTS DIR NOT EMPTY.  DELETE RESULTS DIR TO RUN PERFORMANCE TESTS AGAIN"
	exit -1
fi

echo "Killing NEO4J instance which might be running"
kill -9 `ps | grep neo4j | awk '{ print $1}' | head -n 1`

mkdir $RESULTS_DIR
echo "========= Performance Testing ========" 
for i in `seq 1000 1000 8000`
do
	HOME="$RESULTS_DIR/$i" 
	echo "Running on [$i] nodes.  Results -> [$HOME]"
	mkdir $HOME
	
	# Remove DB
	echo "Removing DB data"
	rm -rf "$NEO_HOME"/"$NE0_DATA_DIR"/*

	# Start NEO4J Server
	echo "Starting NEO4J"
	"$NEO_HOME"/bin/neo4j start 

	# Running python script
	echo "Running python script"
	python ../scenario_b.py "$i" > "$HOME/raw.txt" 
	

	# Kill Server
	echo "Killing NEO4J"
	kill -9 `ps | grep neo4j | awk '{ print $1}' | head -n 1`
	echo "" 	
done
