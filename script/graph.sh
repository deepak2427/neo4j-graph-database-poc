#!/bin/bash
RESULTS_DIR="./results"

echo "========= Performance Testing ========" 
for i in `seq 1000 1000 10000`
do
	HOME="$RESULTS_DIR/$i" 
	echo "Generating Results for $i -> [$HOME]"
	
	if [ -e "$HOME/raw.txt" ]
	then
		MAX=`cat "$HOME/raw.txt" | grep RETRIEVE | sort -nr -k 1 | head -n1 | awk '{print $1}'`
		cat "$HOME/raw.txt" | grep RETRIEVE | awk -v max=$MAX '{ print $1/max"\t"$2}' > "./retrieve.csv"
		gnuplot retrieve.gnu 
		mv ./retrieve.gif $HOME/retrieve.gif
		mv ./retrieve.csv $HOME/retrieve.csv

		MAX=`cat "$HOME/raw.txt" | grep ADD_POST | sort -nr -k 1 | head -n1 | awk '{print $1}'`
		cat "$HOME/raw.txt" | grep ADD_POST | awk -v max=$MAX '{ print $1/max"\t"$2}' > "./add.csv"
		gnuplot add.gnu 
		mv ./add.gif $HOME/add.gif
		mv ./add.csv $HOME/add.csv
	fi

done
