#!/bin/bash
RESULTS_DIR="./results_b"
#NEO_HOME="/home/deepak/Downloads/neo4j-community-1.8.RC1/"
NEO_HOME="/Users/mark.galea/Dev/Tools/neo4j-community-1.8/"
NEO_DATA_DIR="./data/graph.db"

if [ -e $RESULTS_DIR ]
then
	echo "RESULTS DIR NOT EMPTY.  DELETE RESULTS DIR TO RUN PERFORMANCE TESTS AGAIN"
	exit -1
fi

echo "Stopping NEO4J instance which might be running"
kill -9 `ps | grep neo4j | awk '{ print $1}' | head -n 1`
#service neo4j-service stop

mkdir $RESULTS_DIR

# Remove DB
echo "Removing DB data"
rm -rf "$NEO_HOME"/"$NEO_DATA_DIR"/*

# Start NEO4J Server
echo "Starting NEO4J"
#service neo4j-service start
"$NEO_HOME"/bin/neo4j start 

echo "========= Performance Testing ========" 
for i in `seq 1 10`
do
	HOME="$RESULTS_DIR/THREAD_$i" 
	echo "Running thread $i.  Results -> [$HOME]"
	mkdir $HOME
	
	# Running python script
	echo "Running python script"
	python ../scenario_b.py 1000 > "$HOME/raw.txt" &
done

# Kill Server
#echo "Stopping NEO4J"
#service neo4j-service stop
#kill -9 `ps | grep neo4j | awk '{ print $1}' | head -n 1`
#echo "" 	
