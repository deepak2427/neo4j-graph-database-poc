from graph_api import *
import sys
import time    
import random
import string
from contextlib import contextmanager

@contextmanager
def measureTime(count, meta=""):
	t1 = time.clock()
	yield
	t2 = time.clock()
	print '%d %f %s' % (count, t2-t1, meta)


def main():
	g = init_graph()

	if len(sys.argv) < 2:
		print "Provide post count as argument"
		sys.exit(1)

	count = int(sys.argv[1])
	start_string_length = 1000
	end_string_length = 1000
	step_string_length = 1000
	

	#INSERTING DATA
	with measureTime(0, "ADD_USER"):
		add_user("u" + str(0))


	#print "Add second user and insert data..."
	# add first user posts
	for u in range(1, 2):
		with measureTime(u, "ADD_USER"):
			add_user("u" + str(u))

		for t in range(0, 1):
			for p in range(0, count):
				with measureTime(p, "ADD_POST"):
					add_post("u" + str(u), "t" + str(t), "p" + str(p))

	# u1 follow t1
	add_follows("u0", "t0")
	#print "Done..."


	# DUMMY QUERYING TO WARM UP DATABASE
	script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

	params = dict(nodeid=1, tmsi=-1)
	nodes = g.cypher.query(script, params)
	
	script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

	params = dict(nodeid=1, tmsi=-1)
	nodes = g.cypher.query(script, params)



	# QUERYING
	#print "Start querying..."
	for i in range(start_string_length, end_string_length + 1, step_string_length):
	
		# updating the posts with new length
		script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'
		params = dict(nodeid=1, tmsi=-1)
		nodes = g.cypher.query(script, params)
		# creating the string
		post_length = i
		post_string = ''.join(random.choice(string.letters) for i in xrange(post_length-1))
		c = 0
		for n in nodes:
			n.content = "p" + str(c) + post_string[len(str(c)):]
			#print n.content
			n.save()
			c = c + 1

		# retrieve nodes
		with measureTime(i, "RETRIEVE"):
			script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

			params = dict(nodeid=1, tmsi=-1)
			nodes = g.cypher.query(script, params)
			#print len(list(nodes))
			# iterating to access every node's content to just make sure
			for i in nodes:
				if not n.content == None:
					pass


	# TESTING
	script = 'START user=node({nodeid}) MATCH user -[r:follows]-> (tag) --> (post), user -[a?:tmsi]-> post, user -[b?:tm]-> (user_b) WHERE (a.value is null or a.value < {tmsi}) and (b.value is null or b.value < -2) return post ORDER BY COALESCE(a.timestamp?, post.timestamp) DESC'

	params = dict(nodeid=1, tmsi=-1)
	nodes = g.cypher.query(script, params) 
	for n in nodes:
		if not len(n.content) == end_string_length:
			print "Error!!! --- Test Failed!!!"
			exit(1)

	#print g.V
	#print g.E

if __name__ == "__main__":
	main()
