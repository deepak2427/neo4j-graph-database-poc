from bulbs.neo4jserver import Graph
# vertices
from models import User, Tag, Post
# edges
from models import Feed, Created, Follows, Neolink, TMSI, TM
from uuid import uuid4
from sys import exit

def id():
	return str(uuid4().hex)

def init_graph():
	# init the database
	g = Graph()

	# vertices
	g.add_proxy("tag", Tag)
	g.add_proxy("post", Post)
	g.add_proxy("user", User)
	
	# edges
	g.add_proxy("feed", Feed)
	g.add_proxy("created", Created)
	g.add_proxy("follows", Follows)
	g.add_proxy("neolink", Neolink)
	g.add_proxy("tm", TM)
	g.add_proxy("tmsi", TMSI)
	
	return g


# keeping names for easier automation
# todo - change to ids 
def add_post(user_name, tag_name, post_content):
	g = init_graph()
	
	# get the user - todo - use id
	user = g.user.index.get_unique(name = user_name)
	if user == None:
		print "User not found!"
		exit(0)

	# get the tag - todo
	tag = g.tag.index.get_unique(name = tag_name)
	if tag == None:
		tag = g.tag.create(tid = id(), name = tag_name)
		# connect user to tag when it's created
		g.follows.create(user, tag)

	# create the new post
	new_post = g.post.create(uid = user.uid, 
				pid = id(), 
				content = post_content)
	# connect the tag to the post
	g.feed.create(tag, new_post)
	# connect the user to the post
	g.created.create(user, new_post, tid = tag.tid)


def add_follows(user_name, tag_name):
	g = init_graph()
	
	# get the user - todo - use id
	user = g.user.index.get_unique(name = user_name)
	if user == None:
		print "User not found!"
		exit(0)

	# get the tag - todo
	tag = g.tag.index.get_unique(name = tag_name)
	if tag == None:
		print "Tag not found!"
		exit(0)
	
	# connect user to tag when it's created
	g.follows.create(user, tag)



def add_user(user_name):
	g = init_graph()
	# force an error if user already present
	user = g.user.index.get_unique(name = user_name)
	
	# create the user
	user = g.user.create(uid = id(), name = user_name)
	# nasty hack to get reference node
	# g.vertices.get(0) apparently doesn't work
	# this is done only for viewing the graph
	# in neoclipse
	ref = g.V[0]
	g.neolink.create(ref, user)


def get_latest_feed(user_name, count):
	'''
	get user
	get all tag nodes
	get posts in each tag
	merge them
	sort
	return the count
	'''
	

def mark_irrelevant(user_name, post_content):
	g = init_graph()

	# get the user - todo - use id
	user = g.user.index.get_unique(name = user_name)
	if user == None:
		print "User not found!"
		exit(0)
	
	post = g.post.index.get_unique(content = post_content)
	if post == None:
		print "Post not found!"
		exit(0)
	
	g.tmsi.create(user, post)

	#user_post_creator = g.user.index.get_unique(uid = post.uid)
	#g.tm.create(user, user_post_creator)
	
	'''
	get the post node
	check if tsmi edge already exists
		update the score
		get the post's creator id
		update tm score with that user
	else
		create a tsmi edge to the post from user
		get the post's creator id
		create a tm edge with that user	
	'''
